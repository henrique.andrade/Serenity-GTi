json.extract! admin, :id, :membro, :cargo, :saldo, :created_at, :updated_at
json.url admin_url(admin, format: :json)
