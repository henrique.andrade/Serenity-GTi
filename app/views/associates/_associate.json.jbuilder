json.extract! associate, :id, :nome, :email, :cargo, :saldo, :created_at, :updated_at
json.url associate_url(associate, format: :json)
