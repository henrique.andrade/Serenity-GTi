Rails.application.routes.draw do
  get 'welcome/index'

  resources :offices
  resources :associates
  resources :admins
  devise_for :users
  resources :projects
	resources :members
  root to: 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
