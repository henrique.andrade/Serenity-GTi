class CreateProjectsMembers < ActiveRecord::Migration[5.1]
  def change
    create_table :projects_members do |t|
      t.string :project_id
      t.string :member_id
      t.string :position

      t.timestamps
    end
  end
end
