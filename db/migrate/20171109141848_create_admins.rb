class CreateAdmins < ActiveRecord::Migration[5.1]
  def change
    create_table :admins do |t|
      t.string :membro
      t.string :cargo
      t.string :saldo

      t.timestamps
    end
  end
end
