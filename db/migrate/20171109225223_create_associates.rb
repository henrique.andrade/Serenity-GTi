class CreateAssociates < ActiveRecord::Migration[5.1]
  def change
    create_table :associates do |t|
      t.string :nome
      t.string :email
      t.string :cargo
      t.float :saldo

      t.timestamps
    end
  end
end
