class CreateOffices < ActiveRecord::Migration[5.1]
  def change
    create_table :offices do |t|
      t.string :nome
      t.float :porcentagem

      t.timestamps
    end
  end
end
